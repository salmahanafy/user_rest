/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumerge.program;

/**
 *
 * @author salma
 */
public class User {
    public static long LastID = 1;
    private long id;
    private String name, email, address;
    
    User(String name, String email, String address){
        this.id = User.LastID;
        User.LastID++;
        this.name = name;
        this.email = email;
        this.address = address;
    }
    User(){}
    public static void resetLastID()
    {
        LastID = 1;
    }
    public long getID(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
    public String getEmail(){
        return this.email;
    }
    public String getAddress(){
        return this.address;
    }
    
}
