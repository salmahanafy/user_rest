/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumerge.program;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class UserManager {
    private List <User> usersList;
    
    UserManager(){
        usersList = new ArrayList(Arrays.asList(new User("Ahmed","Ahmed@gmail.com","1,1 st"),
                new User("Mohamed","Mohamed@gmail.com","2,2 st"),
                new User("Salma","Salma@gmail.com","3,3 st")));
    }
    
    public List<User> getAllUsers(){
        return usersList;
    }
    public User getUserByID(long id){
        return usersList.stream().filter(user -> user.getID()==id).collect(Collectors.toList()).get(0);      
    }
    public User getUserByName(String name){
        return usersList.stream().filter(user -> user.getName().equals(name)).collect(Collectors.toList()).get(0);      
    }
    public User getUserByEmail(String email ){
        return usersList.stream().filter(user -> user.getEmail().equals(email)).collect(Collectors.toList()).get(0);      
    }
    public User getUserByAddress(String address){
        return usersList.stream().filter(user -> user.getAddress().equals(address)).collect(Collectors.toList()).get(0);      
    }
    public void deleteUserByID(long id){
        for (int i=0; i<usersList.size(); i++){
            if(usersList.get(i).getID()==id){
                usersList.remove(i);
                break;
            }
        }
    }
    
    public boolean editUserByID(User user){
        for (int i=0; i<usersList.size(); i++){
            if(usersList.get(i).getID()==user.getID()){
                usersList.set(i, user) ;
                return true;
            }
        }
        return false;
    }
    
    
}
